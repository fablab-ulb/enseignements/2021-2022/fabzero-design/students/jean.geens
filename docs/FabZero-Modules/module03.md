# 3. Impression 3D

Cette semaine nous avons appris à utiliser les imprimentes 3D.
Suite au module 02, nous avons deja un objet à imprimer. L'objectif d'aujourd'hui va donc être de programmer l'impression.
Nous allons travailler sur les imprimentes du fablab, ce sont des imrimente de la marque "Prusa".
Pour commencer il va donc etre nécessaire d'installer l'application "PrucaSlicer" qui est disponible gratuitement en ligne.

Via le lien suivant;

 [PrusaSlicer](https://www.prusa3d.com/drivers/)

En téléchargeant cette version-ci;

![](../images/prusa-téléchargement.jpg)


## Importer un fichier dans PrucaSlicer;

Pour  cela, il vous faudra engregistrer une copie, de votre création 3D faites sur Fusion, sous le format STL.
Ensuite, en ouvrant "PrucaSlicer", vous allez tomber sur un interface tel que celui-ci.

![](../images/IMPORT-STL.jpg)

aller dans FICHER --> IMPORTER --> IMPORTER STL/OBJ/AMF...


Personnellement, ma pièce est en deux partie, j'importe donc les deux sur PrucaSlicer.

Voici ce que cela donne.

![](../images/fichier-importer-sur-prusa.jpg)


## Procéder aux réglages;

Avant de pouvoir lancer l'impression, il y a différents paramètres a prendre en compte et à régler.
On va donc commencer par sélècionner le bon filamment et la bonne imprimente dans les réglages de la colonne de droite.
Ceux-ci doivent être les même que sur la photo, à savoir;

-Filament: Prusament PLA
-Imprimete: Original Prusa i3 MK3

![](../images/réglage-1.jpg)

On peut observé dans cette même colonne, en bas, un bouton "découper maintenant".
Cette fonction va nous permettre de simuler l'impression de notre objet et de nous donner une idée sur le temps que cela pourrait prendre.
Ici, il nous est demander de ne pas dépasser les 2h d'impression.
Je dépasse donc de trop dans mon cas.

![](../images/découper-maintenant.jpg)


Pour modifier cela, on se rend dans l'onglet "réglage d'impression" en haut à gauche de l'interface.
Plusieur possibilité de réglages s'ouvres à vous.

Dans un premier temps, l'onglet "Couches et périmètres", ou je vais modifier la hauteur des couches et le périmètres des parois

![](../images/modification1.jpg)

Ensuite, dans l'onglet remplissage;

![](../images/modification-2.jpg)

A présent je vais aller voir dans l'onglet "plateau" si le temps à diminué...

![](../images/temps-apres-réglages.jpg)

C'est toujours beaucou trop long!
En fait mon objet est beaucoup trop gros! Et à cette échelle, je vais consommer beaucup trop de plastique.
je vais donc diminué son échelle de 50% et voir les changements.
Je sélèctionne les deux objets à la fois et je modifie leurs échelle de 50%.

![](../images/taille-changée.jpg)

Ce qui va me donner 4h30 de'impression.
C'est toujours trop.
Je prends donc la décision de n'imprimer qu'une seule des deux parties de ma boite.
Je choisi le couvercle, le dessous je le ferais lors de mon temps libre.

Cela me donne;

![](../images/temps-final.jpg)

tout me semblait correcte et je suis dans les temps.
Je décide donc de l'imprimer.
Pour cela;
- Exporter le g-code sur la carte SD en faisant un dossier à son nom.
- Passer un coup sur le plateau avec le produit et papier mis à disposition, afin de le nettoyer.  
- On met la carte SD dans l'entrée prévue à cet effet.
- On selectinne son dossier
- On laisse chauffer l'imprimante le temps nécaissaire.  
- Et on lance l'impression.

Il faut rester lors des 3 premières couches pour être sûr que tout se passe bien, et ensuite on peut simplement venir la rechercher lorsque l'imprimente à finit.

Dans mon cas, tout ne s'est pas passé comme prévu;

![](../images/boite-raté.jpg)


Je me rend compte que j'ai imprimer mon couvercle à l'envers...
Je retourne sur PrusaSlicer et fais faire un 180° à mon objet afin de le retourner;

![](../images/180.jpg)

Ensuite je répète les actions énoncée plus haut pour lancer l'impression.
Apres 1h21 d'impression cela donne;

![](../images/couvercle-ok.jpg)


Je suis revenu plus tard pour lancer l'impression de la seonde partie de ma boite.
J'ai utilisé les mêmes réglages que pour le convercle.
Cela me prit un peu plus de temps, mais voici le résultat;

![](../images/partie-basse-ok.jpg)
![](../images/boite-complette_.jpg)

Voilà ma boite terminer!
