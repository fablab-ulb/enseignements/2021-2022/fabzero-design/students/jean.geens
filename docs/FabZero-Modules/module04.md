# 4. Découpe assistée par ordinateur

Le module de cette semaine nous a appris à utiliser les découpeuses lazer du FabLab. Les découpeuses lazers sont très pratique pour découper avec une grande précision de manièere assez rapide.
Aux FabLab, on peut trouver et utiliser deux machines différentes;

- La lazercutter EPILOG.
- La lazercutter LASERSAUR.

Cette dernière à été construite par le fablab. Elle possède une plus grande surface de découpe que la première.
Afin d'utiliser ces deux machines, on utilisera des programe tel que illustrator ou inskape. Les dossiers devront donc être sous format SVG.

## Familiarisation
Dans un premier temos l'exercices fut de tester les différentes découpes possibles. il est difficile de tester toutes les vitesse avec toutes les puissances. On a donc décidé de travailler sur une même puissance mais de varier la vitesse.
Pour la puissance on a choisis de travailler avec 10W car elle offrait une grande possibilité de gravures et de découpes différentes.

La première étape fut donc de dessiner notre languette sur illustrator, nous aurieons tout aussi bien le faire directement sur inskape.
Voila ce que ca donne.  

![](../images/languette-dessin.jpg)

Une fois que le dessin est terminé, on le depose sur une clée USB qu'on isert nsuite dans "l'ordinateur". Vace notre groupe, nous avons travailler sur la LASERSAUR, c'est donc pour cette machine que je vais expliquer les demarches.

lorsque le dessin est sur l'ordinateur, ouvrez le une dernière fois sur inskape pour vérifier que tout est OK. Dans notre cas, nous avons du modifier l'échelle et les untités (passer de pixels à mm).

Une fois que c'est terminer, on ouvre le dossier dans le logiciel de la lasersaur.
Il se nomme;

```
DriveboardApp.Desktop
```
Comme vous aurez pu le remarquer, chacunes des linges de la languette a une couleur différente. Ces couleurs vont permettre à la machine de différencier les découpes. Dans la colonne de droiteb=, on selectione alors une couleur (en premier les gravure, les découpes à la fin) à laquelle on attribue une puissance et une vitesse.
Dans notre cas, on travaillera toujours avec 10W comme on l'a dit plus tôt.

![](../images/donné-découpes-1.jpg)

On ouvre ensuit ela machine, on place le matériel que l'on veut veux découper à l'intérieur. on le stabilise grâce à des poids.

![](../images/stabilisation-découpe.jpg)

On referme la machine et on peut lancer la découpe en appuyant sur "RUN"
Voici le résultat;

![](../images/languette-fin.jpg)


## Création dune lampe grâce à la lazercutter

Après avoir prise en mains les lazercutter, la consigne etait de fair une maquette avec une feuille d'un abbat-jourpour une source lumoneuse  que nous devions apporter. Celle-ci ne devait se composer que d'un soquet et de la source de lumière, à savoir l'ampoule.

![](../images/source-lumineuse.jpg)

Pour nous inspirer, les abbats-jours devaient être en lien avec l'objet que nous allons créer. Etant donné que je travail sur le sujet de la peche, je me suis ispirer du dessin classique, enfantin d'un poisson pour composer ma lampe.

![](../images/essaies.jpg)

L'idée était de faire des plis sur les contour du poisson pour arriver à l'enrouler.
J'ai donc fais des essaies sur le papier.

A se moment la j'étais assez satisfait des recherches et des essaies que j'avais fait. j'ai donc décidider de le dessiner sur illustrator.

![](../images/premier-abbat-jour.jpg)

L'avantage de travailler au Fablab, c'est de pourvoir échanger avec les autres. Apres des discussion aussi bien avec les encadrants et profs que'avec les autres étudiants j'ai décidé de remodifier mon dessin. Le but étant de plus affirmer la silouette du poisson et de la rendre plus visible.
Apres plusieur essaies, j'en suis venu à un résultat qui me convenait parfaitement.
Je l'ai donc redessiné;

![](../images/dessin-final-abbat-jour.jpg)

Je suis donc passé à la découpe de mon objet. les couleurs sont deja mise, je n'ai plus qu'a;

```
- Vérifier l'echelle sur inskape
- Importer le dessin sur DriveboardApp.Desktop.
- indiquer la vitesse et les W pour chaques couleurs, les gravures (en rouge) seront à 10 W pour une vitesse de 1000. les découpe seront faite à la puissance de 35W pour une vitesse de 1200.
Je n'oublie pas de commencer par les gravure avant les découpes pour éviter que l'objet bouge dans la machine.
- Je lace ensuite le papier dans la machine que je fais tenir grace aux poids.
- Je vérifie avec les deux flèche en haut a gauche (à coté de "run"), que la découpe rentre bien sur la feuille.
- lorsque tout est bon je peux lancer.
```
Après quelques minutes, voilà la découpe terminée. le résultat est le suivant.

![](../images/découpe.jpg)

Une fois assemblé avec ma source lumineuse;

![](../images/lampe-finie.jpg)

Je suis content du résultat, les formes de poisson sont bien visible! les nageoires, qui serve de pied accentue d'autent plus leurs forme. Et les écailles donnent de légers reflets qui rappels la brillance d'un poisson.   
