# Module 1 ; Git

Pour ce premier module, je vais vous montrer comment installer et paramétrer Git.
Dans un second temps, grâce à une clé ssh,  je vais expliquer comment cloner une copie de votre Git sur votre ordinateur.
Et enfin pour finir je vais vous montrer comment ajouté du contenu sur votre site via le logiciel atom.

## 1) installation de Git;
Dans un premier temps je tiens à spécifier que j'effectue mes manipulations sur Windows. Celles-ci peuvent être différente que celles à effectuer sur mac.

Tout d'abord il faut ouvrir votre terminal dans la barre de recherche et vérifier si Git est déja installer sur votre ordinateur.
Pour cela, on utilise les commandes suivantes;

```
Microsoft Windows [version 10.0.19042.1237]
(c) Microsoft Corporation. Tous droits réservés.

C:\Users\gjean>git --version
git version 2.33.0.windows.2
```
Pour ma part Git est déjà installer sur mon ordinateur. Si ce n'est pas le cas pour vous, je vous invite à aller le télécharger sur le lien suivant.

[instal Git Bash](https://git-scm.com/download/win)


## Configurer Git;

Dans un second temps, on s'identifie avec son nom et une adresse mail (qui correspond a celle de votre site).

```
C:\Users\gjean>git config --global user.name "jean.geens"
C:\Users\gjean>git config --global user.email "Jean.Geens@ulb.be"

```

Afin de visualiser si les données sont correct, on effectue;


```
C:\Users\gjean>git config --global --list
user.name=jean.geens
user.email=Jean.Geens@ulb.be

```

## Générer une clé ssh;
Une clé ssh va être indispensable pour lier votre ordinateur à votre site. Normalement il n'y a pas encore de clé crée sur votre ordinateur.
On va donc crée une clé ssh ed25519.
Sur votre terminal, taper;

```
C:\Users\gjean>ssh-keygen -t ed25519 -C "Jean.Geens@ulb.be"
Generating public/private ed25519 key pair.
Enter file in which to save the key (C:\Users\Jean.Geens/.ssh/id_ed25519):

```
On appuie sur "enter" et notre clé est crée.

Pour que la clé soit correctement générée, on passe sur l'application Git Bash que nous avons installé plus tôt.
On y écrit;

```
jeangeens@MSI:/mnt/c/Users$ cat ~/.ssh/id_ed25519.pub
ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAICAn/Sx1PP327KkSb6202C7F6su6K7m+acDo2nkqBni/ clé

```
Maintenant uil s'agit d'aller ldéposer sur votre compte Gitlab.
Pour cela je vous invite à suivre les indications suivantes.

![](../images/préférence-ssh-1.jpg)
![](../images/préférence-ssh-2.jpg)
![](../images/préférence-ssh-3.jpg)

A présent, nous devons cloner cette clé ssh. Pour cela on se rend sur notre GitLab dans l'onglet suivant;

![](../images/clonage-1.jpg)

On retourne sur son terminal et on indique les données suivantes "git clone" suivi du lien encadré en rouge ci-dessus.
Cela donne;

```
C:\Users\gjean> git clone git@gitlab.com:fablab-ulb/enseignements/2021-2022/fabzero-design/students/jean.geens.git
Cloning into 'jean.geens'...
The authenticity of host 'gitlab.com (172.65.251.78)' can't be established
ED25519 key fingerprint is SHA256:eUXGGm1YGsMAS7vkcx6JOJdoGHPem5gQp4taiCfCLB8.
This key is not known ny any other names
Are you sure you want to continue connecting (yes/no/fingerprint)?

```
On écrit "y" puis on fait "enter"
Votre terminal va ensuite vous demander de confirmer en indiquant votre de passe.
Après l'avoir inscrit, on appuie sur "enter" et le tour est joué!


## Compléter votre site avec "Atom";

Atom est un lociel gratuit qui vous permettra facilement d'éditer des textes pour completer votre site.
Vous pourrez le télécharger via le lien suivant. il est gratuit et très facile d'utilisation.

[Atom](https://atom.io/)

Une fois Atom installer, vous arriverez sur une interface tel que celle-ci. En cliquant sur "open a Project", vous pourrez ouverir le dossier dans lequel vous allez travailler. Le suivant sera détaillé dans l'onglet "Project" à droite.

![](../images/Atom.jpg)

il est ainsi facile de jongler entre les différents modules, il est aussi possible de traivailler sur plusieurs dossiers en même temps.
les fichiers sont directements modifiable sur Atom. vous pourrez y ajouter des liens vers des sites internets ou des images.


## Ajouter le dossier à votre site;

Lorsque le document terminé et enregistré, je vous invite à ouvrir votre terminal.
Dans le dossier correspondant vous pourrez enregistrer les modifications avec la manipluation "Git add -A".

La commande "git commit -m "nom du dossier"", vous permettera d'envoyer toutes les modifications vers GitLab.

Pour vérifier l'avancement de votre dossier, vous pouvez écrir "git status" dans le terminal.

Pour envoyer le dossier avec les nouvelles données, il suffit de taper "Git push".

A moins d'avoir commis une erreur, votre site sera modifié!!

A vous de jouer maintenant!
