# 2. Conception Assistée par Ordinateur

Cette semaine, je vais expliquer comment concevoir un objet en 3D via le programme Fusion 360.
Pour rappel, le sujet sur lequel je vais travailler est la peche.
Ce qu'il me manque c'est du rangement!
Etant donné que je ne suis pas encore sur de la forme que mon objet va prendre, j'ai décider de travailler sur une boite pour y mettre mon matériel.


## Conception

je prends le parti de faire une boite compact. Ainsi elle sera facile à transporter et à ranger dans mon sac a dos.
Elle se divisera en 2 parties, le couvercle et la boite en elle même. Cette dernière sera séparée en 3 comparitiments afin de pouvoir y ranger des accessoires différents.

## Conception de la première partie de la boite;

En ouvrant le programme, une page tel que celle-ci va apparaitre.
La première étape est de positionner la caméra sur la face du haut du petit cube (en haut à droite).
Ensuite on appuie sur l'interface "esquisse".

![](../images/fusion-1.jpg)

Je selectionne l'outils "rectangle" pour dessiner un quadrilataire d'une longueure 150mm et d'une largueur de 100mm.

![](../images/FUSION-2.jpg)

Cette dimension me parait assez résonnable. je vais maintenant donner une épaisseur au fond de ma boite.
Pour cela, il vous suffit de suivre les indication suivantes.

1) Séléctionner l'outil "extrusion"
2) Indiquer la hauteur que vous désirez.
3) Appyez sur "enter"

![](../images/FUSION-3.jpg)

La base de notre boite est dessinée.
Maintenant on dessine les formes que les parois prendront sur le socle que nous venons de créer.
Pour cela, il faut créer une nouvelle esquisse (tjs vue du haut) sur notre base existante.
Je décide de créer 3 compartiments à ma boite, je calcule donc rapidement les dimesions de ceux-ci. En sachant que je voudrais donner aux parois une épaisseur de 6mm afin que la boite soit assez résistante, les compartiments feront 42mm sur 88mm.
Je les dessine de nouveau grâce à l'outil "rectancle"

![](../images/FUSION-4.jpg)

On extrude;

![](../images/FUSION-5.jpg)


On recommence l'oppérassioon afin de faire une petite encoche dans les parois et ainsi y fixer notre couvercle.

1) On crée une esquisse
2) Utiliser l'outil "restangle" pour faire un contour de 4mm de largeur sur le dessus de la boite.
3) on extrude de 6mm.

![](../images/FUSION-6.jpg)
![](../images/FUSION-7.jpg)

Voila la partie basse de notre boite terminée. on passe au couvercle.

## Couvercle de la boite;

On commence par ouvrir le dossier corps qui se trouve sur la droite de notre écran pour renommer le dossier deja existant et on y note "boite".
On ouvre un nouveau dossier sur lequel nous allons travailler.
Nous l'appelerons "couvercle".

![](../images/fusion-7.5.jpg)

Dans ce dossier, nous allons dessiner le dessus de la boite.
On commence comme pour la première partie de ce module.
On fait un rectangle de la même dimension que notre boite;

![](../images/FUSION-8.jpg)

On l'extrude

![](../images/FUSION-9.jpg)

Je prends la décision de redessiner les parois intérieurs pour donner plus de profondeur et éviter que mes plombs ou hameçons ne se dispercent dans la boite.

Retracer donc les parois intérieurs normalement, contrairement aux exrtérieur qui doivent laisser un retrait de 4mm pour s'emboiter avec l'autre pièce de notre boite.  
cela donne.

![](../images/FUSION-10.jpg)
![](../images/FUSION-11.jpg)

On extrude

![](../images/FUSION-12.jpg)

la couvercle est presque finit!!

## Dernière touche;

Voilà la forme de notre boite est terminée.
Je décide cependant d'y ajouter quelques "décorations".
je vais noter mon nom sur le dessus de la boite.

Sur mon cube en haut à droite, je clique sur la face du bas et crée une nouvelle esquisse.

![](../images/FUSION-13.jpg)   

Je prends l'outils texte et désigne la zone où je veux écrire.

![](../images/FUSION-14.jpg)

Ensuite j'extrude en négatif pour donner un effet de gravure.

![](../images/FUSION-15.jpg)

Je vais aussi dessiner un logo de poisson.

1) Je refais une esquisse sur cette même face.
2) J'utilise l'outil "arc de cercle en 3 points" et je trace celui-ci à l'endroit voulu.
3) Une fois un arc dessiné, je trace un axe entre les deux extrémitées.
4) J'utilise l'outil "symétrie" autour de l'axe que je viens de tracer. Cela me permet d'avoir deux arcs exactement pareils.  
5) Pour la queue je trace simplement un triangle isocèle dont le sommet rejoint les deux arcs.
6) Finalement j'extrude le tout aussi en négatif, pour le même effet de gravure.   

Et voila le résultat final!

![](../images/FUSION-17.jpg)

La boite est terminée et n'a plus qu'a être imprimée.
