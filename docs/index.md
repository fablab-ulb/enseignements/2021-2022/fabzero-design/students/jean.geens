![](images/photo-avatar-comp.jpg)





## A propos de moi;

Hello!

Je m'appelle Jean Geens, j'ai 22 ans.
A l'heure actuelle, j'entame ma première année de master 1 en architecture, à l'université libre de Bruxelles.
Mes centres d'intérêts principaux pour le moment sont, la décoration ou l'aménagement, les films et pendant mes temps libre, j'aime pratiquer la pêche dans l'ancien canal Bruxelles-Charleroi. C'est des moments que je passe assez souvent seul ou avec des amis et qui me permettent de me retrouver.


## Mon parcours

J'ai passé mon enfance dans la ville de Genappe ou j'ai suivis mon enseignement primaire. C'est à l'Enfant-Jésus, à Nivelles, que j'ai obtenus mon diplôme de CESS.
Après la rétho, je suis partit pendant un an voyger. Ma destination était la Nouvelles-Zélande, pour ses paysages époustouflants et la sympathie des gens qui y vivent.

En 2018, je suis rentré à l'ULB en architecture. Cela fait donc trois ans que je développe mon sens artistique et mon gout pour l'architecture.   



### Mon projet

L'objet sur lequel j'aimerais travailler au cours de ce quadrimestre, est "sac" et objet de rangement pour mon matériel de pêche.
Le problème auquel je suis confronté lorsque je vais pêcher est que j'y vais généralement en vélo. Comme on peut se douter, du matériel que j'emporte avec moi est assez encombrant.
De même, lorsque qu'il est stocké chez moi, il prend beaucoup de place.
J'aimerais donc fabriquer un sac dans lequel tout le matériel rentre, dans le quelle je pourrais laisser mes affaires sans que cela ne prenne trop de place, aussi bien physiquement que visuellement.
