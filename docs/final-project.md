# Final Project



# PREMIERE SEMAINE:
À travers cet exposé, nous avons compris la différence entre signe et fonction de par la référence cinématographique "mon oncle", où les objets de design sont caricaturés. Ils sont représentés comme étant fragiles et esthétiques mais manquant d'aspect pratique. Il s'agit donc d'une critique du Design, dans laquelle on exprime que les objets ne servent plus à rien.

Distinction entre signes et outils.

Lors de la présentation Victor Levy a abordé également le concept des 6 étapes de conception d'un objet design :

- 1: Déconstruction de la fonction de l'objet.

Regarder de l'extérieur, il faut distinguer l'objet de sa fonction, il faut le regarder, analyser l'objet, le comprendre, comprendre son utilité.

- 2: Résoudre un vrai problème.

Savoir ce que l'on veut améliorer, comprendre ses contraintes et ses limites. Ce sont elles qui font l'objet. On tire une conclusion sur ses faiblesses.

- 3: Comprendre les dimensions spécifiques.

il faut penser en ergonome, au confort de l'objet, par exemple, mais aussi à son coût et à la qualité de matière que celui-ci consomme. Il faut adapter le produit à son consommateur. C'est son utilité qui en déduira sa forme.

- 4: Esquisses, essais et erreurs.
Il faut tester et demander des avis. C'est là que se joue les décisions techniques. Il est encore temps de revenir en arrière si nécessaire.

- 5: Adaptation, diffusion, édition.

L'objet est unique, il reste reproduisible grâce à la documentation qui a été faite tout au long de la création. L'objet à été réduit à sa plus simple expression, en utilisant le moins de matière possible.

- 6: Trouver un nom, un pictogramme.

- 7: Lancement du projet.



Dans la seconde partie du cours, nous avons fait des équipes de trois pour essayer d'effectuer les trois premières étapes de la list ci-dessus.


# Etape 1 ; Déconstruire la fonction de l’objet :

Un bon sac de pêche doit avant tout être confortable ! Il doit être compact afin de ne pas gêner le pêcheur dans ses mouvements. On doit aussi pouvoir y accéder facilement et adroitement pour ne pas perdre trop de temps. Les espaces de rangement doivent donc être bien organisés. Chaque objet doit avoir une place spécifique.

Les 4 caractéristiques principales sont :
- Légèreté
- Contient tout le matériel nécessaire
- Agréable à porter et ne pas gêner l'utilisateur : confortable



# Etape 2 ; Résoudre un vrai problème.

Le matériel de pêche est généralement volumineux. Il est donc très difficile à transporter sans véhicule, ce qui est mon cas. En Belgique et plus précisement dans le Brabant-Wallon, les spots de pêche sont peu nombreux. De même à Nivelles, ville dans laquelle j'habite. Le spot le plus proche et celui où je me rend le plus souvent, est l'ancien canal Bruxelles-Charleroi. Le trajet à vélo se fait en +/- 30min tandis qu'en voiture il se fait en 20min. J'apprécie d'y aller à vélo car cela me permet de me dépenser. Je me retrouve donc la plupart du temps avec mon sac de cours sur lequel j'essaye d'accrocher tant bien que mal ma canne à pêche. J'essaye d'y insérer aussi ma sacoche contenant tout mon matériel. Je dois tout de même garder mon sac sur le dos en arrivant, malgré la sacoche en bandouillère. Cela fait donc beaucoup de matériel à porter pour une session de pêche qui peut parfois durer plus de 5h.
Autant dire que le confort n'est pas au rendez-vous!;)


En résumé pour combler mon soucis de confort et de surcharge, l'objet que j'aimerai créer serait un sac/sacoche ou autre bagage qui me permet de me déplacer confortablement et de ne pas gêner durant les sessions de pêche. Je pense qu'un sac unique, rassemblant tout le matériel nécessaire et le rendant accessible rapidement serait l'idéal. Le but serait aussi de transporter uniquement le matériel nécessaire, car j'ai tendance à toujours utiliser que la moitié de ce que j'emporte avec moi...

Les points les plus important à mettre en avant dans mon projet sont :
-La légèreté
-Un contenant pour le matériel indispensable
-Confortable  


# Etape 3 ; Comprendre les dimensions principales.


Le bon "sac" de pêche à mes yeux devra être compact sans pour autant être avare d'espaces de rangements. La pêche est un loisir qui comprend beaucoup de matériel. Evidemment avant chaque session, il faut se préparer et prévoir le bon matériel pour un type de pêche bien précis.

Généralement pour partir je prends le matériel suivant;

- Canne
- 2 boites pour les leurres et le petit matériel.
- Une bobine de fil.
- Une paire de ciseaux.
- Une pince de chirugien

Voici leurs dimensions;

![](./images/dimension.jpg)

Pour concevoir mon sac, je me suis basé sur mes propres dimensions corporelle.

![](./images/dimension_corps.jpg)

il est important de connaître ces dimensions car je me déplace à velo la plupart du temps. Cela me permettra aussi de décider l'emplacement de ma canne, qui est l'objet le plus encombrant dû à sa longueur qui est de 1m. Celle-ci ne doit pas me déranger quand je roule à vélo ou lorsque je marche.

# Etape 4 ; Esquisses.

## Esquisse 1
Pour mes premières esquisses, j'ai pensé à un objet assez volumineux qui tiendrait dans le dos, de la même manière qu'un sac mais à l'allure d'une petite étagère (cf croquis dessin de gauche). Celui-ci serait confectionné en bois léger : contreplaqué très fin.

![](./images/esquisse-1.jpg)

## Esquisse 2

Après en avoir discuté avec d'autres élèves, la forme (de face) plutot carré de l'objet s'opposait à la verticalité de la canne. J'ai donc gardé la même idée en augmentant sa hauteur. (Cf croquis de droite, une image plus haut) Voici plus de détails et d'explication :

![](./images/dessin-esquisse1.2.jpg)
![](./images/expliquation-esquisse-1.2.jpg)

Le principe de ces 2 premiers modèles est qu'ils auraient pu facilement être déposé sur mon vélo, sur le sol ou encore chez moi. Le matériel resterait également rangé. Le contenu du sac serait facilement accessible tel une armoire. Différentes contraintes se présentaient dans la réalisation de l'objet. Je me suis donc posée les questions suivantes, mettant les caractéristiques principales en avant :


-Est-ce réellement plus légé que la solution actuelle, à savoir: un sac a dos + sacoche et matériel?
-Est-ce que ce sac sera confortable?
-Est-ce réalisable avec la qualité recherchée?
-Est ce qu'il est possible de simplifier cet objet?


J'ai donc repensé cet objet pour le simplifier au maximum.
Le principe se baserait sur un système où les boites et le reste du matériel seraient à l'air libre, attaché directement à la structure que je porterai sur le dos.
Ainsi, je n'aurais plus à ouvrir de poches pour accéder à mes affaires.
Voici un schéma illustrant mes propos;

![](./images/esquisse2.jpg)

## Esquisse 3

La solidité du modèle précédent ne me convenait pas. L'ajout de différents éléments auraient du venir compléter le sac afin d'avoir un objet convenable. J'ai donc simplifié le projet en me basant sur un unique matériel de confection. A savoir un sac unique, à porter en bandouillère et assuré par un système de ceinture. Le but de cette bandouillère est de la rendre à la fois compacte, pratique et accessible. Celle-ci pourrait venir se glisser du dos vers l'avant, afin d'accéder facilement à toutes les poches dont chacune sera dédié à un ustensile bien précis.

![](./images/ESQUISSE3.jpg)
![](./images/ESQUISSE3.2.jpg)

## Materials

| Qty |  Description    |  Price  |
|-----|-----------------|---------|
| 2m² | Simili cuir     |  Récup  |   
| 2,5m| Sangles         |  Récup  |    
| 2   | Tirettes        |  Récup  |    
| 1   | Fil de couture  |  Récup  |  
| 1   | Aiguille        |  Récup  |    
| 1   | Machine à coudre|  Récup  |   



# Confection de l'objet

Etape 1 : coudre les sangles ensemble.

![](./images/etape1.jpg)

E2 : Dessin et découpe du premier patron.

![](./images/etape2.jpg)

E3 : Dessin et découpe du deuxième patron.

![](./images/etape4.jpg)
![](./images/etape5.jpg)

E4 : Attacher les sangles au premier patron.

![](./images/etape3.jpg)
![](./images/etape3.2.jpg)

 Error : Les deux patrons ne correspondaient pas.

E5 : Ré-ajuster le deuxième patron.

![](./images/etape6.jpg)

E6 : Découpe des poches.

![](./images/etape7.jpg)

E7 : Couture des poches.

![](./images/etape8.jpg)
![](./images/etape9.jpg)

E8 : Coudre les poches ensemble.

![](./images/etape10.jpg)

E9 : Attacher les deux poches au premier patron, sur la base de la sacoche, à l'arrière des sangles.

![](./images/etape11.jpg)

E10 : Reprendre le deuxième patron, découper l'entrée des poches à la bonne hauteur et coudre les tirettes aux poches.

![](./images/etape12.jpg)

E11 : Attacher les patrons ensemble et coudre les poches aux tirettes.

![](./images/erape13.jpg)

E12 : Du à mon erreur entre l'étape 4 et 5 un petit ajustement était nécessaire. J'ai du redécouper un
morceau de simili cuir pour finaliser mon objet.

![](./images/etape14.jpg)


Voici le résultat final :

![](./images/rendu1.jpg)
![](./images/rendu2.jpg)
![](./images/rendu3.jpg)
![](./images/rendu4.jpg)
![](./images/rendu5.jpg)
![](./images/rendu6.jpg)
![](./images/rendu7.jpg)


## Points faibles :

- Les coutures
- Système de fixation
- La taille de l'objet (trop long)
- Ceinture et bretelles mal ajustés
- Manque de système de fixation pour la canne
- Absence de poche pour matériel : pinces,ciseaux,bobine, etc..

Remarques :

Pour la prochaine fois :
- Faire un seul patron
- Attacher les poches entre les tirettes et le tissu
- Autres dimensions de poche (plus longue)  

# Esquisse 4, prototype

- Materials

| Qty |    Description    |    Price    |
|-----|-------------------|-------------|
| 2m² | tissu imperméable |  14eu / m²  |   
| 2,5m| Sangles           |  5,85e      |    
| 4   | Tirettes          |  7eu        |  
| 2   | attaches          |  4eu        |  
| 1   | Fil de couture    |  Récup      |  
| 1   | Aiguille          |  Récup      |    
| 1   | Machine à coudre  |  Récup      |

Pour la continuité de mon projet, je suis resté sur la même idée en apportant différentes amélioration;

- Les poches ont changées de sens, elles ne seront plus a la vertical mais à l'horizontal.
- Les les tirettes aussi seront changées, elles seront mise a la vertical et non plus à l'horizontal. Elles seront disposées du coté intérieur de la sacoche.
- Un rangement pour la conne sera aussi prévu via un système de rabat.
- Il y aura deux poches en plus, une sur la ceinture afin de ranger mes ciseaux, la pince, la bobine de fil. L'autre en dessous des boites, pour pouvoir y mettre des effets personnels.
- Les matériaux aussi vont changer, je vais en tester de nouveaux.

![](./images/croquis1-esquisse4.jpg)
![](./images/croquis2-esquisse4.jpg)

Etape 1: Dessin et découpe du patron sur du papier calque semi-transparent. J'en profite pour vérifier les dimensions et être certain que les boites rentrent dans le futur sac. Je marque aussi les emplacements des tirettes.

![](./images/patron-papier.jpg)
![](./images/patron-papier2.jpg)

Etape 2: Attacher le patron sur les tissus, afin de pouvoir découper le tout.

![](./images/partronettissus.jpg)
![](./images/détails-patronettissus.jpg)

Etape 3: Découper le tissu:

![](./images/découpe1.jpg)

Etape 4 : Je découpe les sangles sur une longueure de 55cm chacunes. Je viens ensuite les coudre sur le coté intérieur du tissu imperméable (le bleu). j'en profite également pour y attacher les tirettes.

```
La technique utilisé pour coudre consiste à coudre le tout à l'envers et à retourner le tout pour éviter que les coutures ne se voit.
On coud ainsi les tissus au tirette en mettant la doublure vers l'extérieur.
Une fois les coutures faites, je retourne le tout pour que les tissus soit du bon coté.
cette technique sera utilisé tout au long de l'assemblage du prototype.

```

Etape 5 : Confection de la ceinture, on retrace un patron de celle-ci que l'on découpe. On coud les deux partie ensemble suivant la technique expliquée plus haut. On y insert aussi la sangle.

![](./images/ceinture.jpg)

Etape 6 : On joint la ceinture et la sacoche ensemble.

Etape 7 : j'avais prévus de coudre une doublure à l'intérieur de la socoche mais apres réflèction je ne suis pas certain de son utilité. Cela utilise de la matière supplémentaire et pas nécessaire.

Etape 8 : Le sac est prêt à acceuillir les boites de peche mais pas encore la canne. Pour cela, je conçois un "rabbat", prévu pour recouvrir la canne et la protégée. Pour que la canne tienne correctement, je fabrique un fourreau;


## retour sur le projet, auto-critique et teste du second prototype;


 1) Autocritique:


    - Améliorer le support de Cannne.
    - Créer une poche en plus pour le petit matériel, ciseaux, bobine, telephone, etc...


 2) Retour sur le pré-jury:


    - Le projet doit être numériser, utilisation des appareils mit à notre disposition.
    - Améliorer le système d'attache pour la canne.
    - Etanchéité?
    - Tissus est-il le bon?
    - Comment prendre la canne?


3) Teste:


    - Manque d'une petite poignée pour prendre le sace lors de petits déplacement mais aussi pour l'attacher, dans le train, au porte-manteau ou dans un arbre pour faire une pause.
    - Espace pour l'eau.
    - Petite poche "loisir" et petits matériels.
    - Support de canne est un réel problème.
    - Besoin de rigidité.
    - Revoir les mesures.
    - soliditée.
    - Confort, les sangle remonte sur le ventre et ne reste pas sur la taille, ce qui ne garentit pas un confort constant.


# Teste sur le prototype;

Pour effectuer des teste, je me suis servis du dernier prototype. En détachant certaine partie de celui-ci et en les replaçant, dans le but de garantir un confort plus adéquat.
J'ai donc découpé la partie comprenant la sangle du bas ainsi que la partie "ceinture" qui comprend la sangle latéral.
en effectuant plusieur testes, voici les découpes effectuées.

![](./images/Ddecoupe-prototype.jpg)

Après plusieur essaies la disposition la plus adéquate m'a parut être celle-ci;

![](./images/retouche-sac1.jpg)
![](./images/retouche-sac2.jpg)

Avec cette disposition, la sangle du bas se retrouve elle aussi sur le coté pour revenir sur l'avant du corps de manière latéral. Ainsi les attaches ne se trouvent plus sur les hanches mais au niveau du sternum.  
Cela ne semble pas gêner sur un velo non plus, au contraire, le sac revient plus haut. Cela permet à la canne de ne jamais toucher les roues, lorsque que l'on se tient droit ou mêm en position penché, lorsque l'on roule à vélo par exemple.


![](./images/test-velo1.jpg)
![](./images/teste-velo2.jpg)

le sytème de maintien de la canne à lui aussi été amélioré simplement en permettant à la poignet du moulinet de dépasser par le voile de protection. la canne ne sera plus tenue par le fourreau mais part le moulinet ce qui offre plus de flexibilité quant au taille de canne à transporter.

![](./images/teste-moulinet.jpg)


J'ai donc décider de me baser sur ce modèle pour concevoir le prototype suivant.


# Dernier prototype;

- 1) Le patron:

La première étape fut, comme pour le prototype précédant, de dessiner le patron du sac. lors du dessin de celui-ci, il fallait bien faire attention à prendre en compte les remarques du pré-jury ainsi que les éléments qui posaient problème lors du teste effectué.
Il a donc fallut prendre en compte de l'esace supplémentaire pour une poche sur la ceinture de droite, un espace pour l'eau qui sera disposé dans le dos grâce à un camel-back.
Pour le système d'attache de canne, le voile, qui vient recouvrir la canne sera mieux adapté grace à l'étude mené sur le prototype précédant. D'autres modifications et améliorations seront mis en place mais pas par le patron. Nous y reviendrons par après.

![](./images/patron-final.jpg)

- 2) Le tissu:

la deuxième étape fut de trouver le bon tissus pour réaliser le sac. Celui-ci doit etre étanche, solide et éventuellement apporter de la rigidité. lors du pré-jury, on m'avait proposer de travailler avec des tissus d'affiche de pub par exemple.
Gâce à un amis qui travail dans une entreprise de location de tente, j'ai pu récupérer des chutes de baches.
Celle-ci semblaient répondre parfaitement à ce que je recherchais!
Différent type de baches étaient disponibles de même que des couleurs mais j'ai décider de rester sur des ton simple. J'ai donc pris du noir ainsi que quelque morceux de couleurs.

![](./images/différent-tissus.jpg)

- 3) le matériel:


| Qty |    Description    |    Price    |
|-----|-------------------|-------------|
| /   | Baches de tentes  |  Récup      |   
| 2m  | Sangles noirs     |  5,85e      |    
| 4   | Tirettes          |  7eu        |  
| 4   | Attaches          |  4eu        |
| 2m² | Tissu doublure    |  7eu/m²     |
| 1   | Fil de couture    |  Récup      |  
| 1   | Aiguille          |  Récup      |  
| 1   | Camel-back        |  Récup      |  
| 1   | Machine à coudre  |  Récup      |



- 4) La découpe:

Afin d'optenir les découpes les plus parfaites possible et de gagner un maximum de temps, j'ai décider de découper le tissu grâce à la lazersaure. Les découpes lazer sont bien plus nettes et précises que les découpes à la main qui sont plus tremblantes.
Dans un premier temps, j'ai donc effectué une découpe teste. le but étant de savoir quelle vitesse et quelle puissance seraient les plus adéquat pour se type de matière.
Voici ce qui en est ressortit;

![](./images/découpe-teste.jpg)

1) les bonnes découpes:
                          - 4W/100mm
                          - 6W/100mm
                          - 6W/300mm
                          - 10W/100mm
                          - 10W/300mm
                          - 10W/600mm

2) Les bonnes gravures:
                          - 4w/2300mm
                          - 6w/1200mm
                          - 6w/2300mm



je n'effecturerais pas de gravues, seulement des découpes.
Deuxièmement, j'ai redessiné le patron via Autocad, cela me permet aussi d'etre plus précis que le dessin et la découpe à la mains.
Pour effectué mes découpes j'ai choisi de programmer la machine sur 10W/600mm cela m'a permit d'avoir une vitesse de découpe respectable tout en ayant une découpe bien nette.
Voici ce qui a été découpé;

![](./images/découpes.jpg)

en plus de cela, des languettes ont été découpé dans des baches oranges, elles serviront pour former des attaches sur le sac.


- 5) Confection du sac.

La confection du sac a été asez fastidieuse car les bashs bien que partique sur plusieurs aspects, sont très difficile à travailler. la même technique que pour le prototype précédents a été utilisée. C'est a dire qu'il faut coudre le tout à l'envers, avant de retourner la conception afin que les coutures soient cachée.
Les baches étant assez rigides les confections étaient très compliqué à retourner!

Le sac (sans les ceintures) sera composé de plusieur couches, en partant du coté du dos, nous aurons:

- Une bache
- Du rembourage
- de la doublure

Ces trois matéiaux seront cousu ensemble. Celui-ci correspond a la couche 1.

- bash
- Rembourage
- Doublure

Eux aussi seront cousu ensemble. Ce sera la couche 2.
Entre les deux premiers assemblages se trouvera le Camel-back.

- Doublure
- Bache  

Ce dernier assemblage sera la couche 3.
Entre ces deux derniers assemblage se trouveront les poches pour les boites de leurres.

Etape 1:

la première étape fut de coudre la partie du bas du sac, à savoir de la bash (prédécoupé) avec du molletonage et la doublour (1er assemblage). La douloure et le moltonnage serotn coupé suivant la forme des baches.
Il a aussi fallut prévoir l'ouverture pour le camel-back. On y accèdera grace à une tirette qui se disposera contre le dos afin que cette ouverture soit discrète.

![](./images/couture-moltonage-dos.jpg)
![](./images/tirette-dos.jpg)

Une fois cela effectué, on repasse l'ensemble pour éviter un maximum de plis.

![](./images/repassage.jpg)

Pour finir on fais trois lignes de coutoures afin que le rembourage tienne bien contre le dos.


Etape 2:

On forme le deuxième assamblage, on coud ensemble la bache, le molletonage et la doulure.
On forme aussi le troisième assemblage. Doublure + bash.

Error: l'un des assemblage à été fait à l'envers.

![](./images/3couches.jpg)

Etape 3:

Entre temps on vient coudre sur les bandes des poches, les tirettes. Pour cela on fait une coupe de la longueure desirée et on vient attacher les tirettes du côté intérieur du sac.
L'épaisseur des poches sera de 4cm mais nous devons faire attention à prende de l'espace suplémentaire pour venir attacher ces bandes au rest du sac.

![](./images/tirettes-poches.jpg)

Error: les tirette sont trop longues et ne se modulent pas comme voulu à cause de la rigidité des baches.

![](./images/error-tirette.jpg)


Solution: pour Répondre aux 2 problèmes rencontrés, il a fallut agrendir la saccoche de 5cm. Pour cela j'ai du redessiner la couche 3 en rajoutant de la longueure. Les 5cm nécessaire plus ceux manquant à la couche 1 et 2cm pour effectuer les coutures sur chaques couches.

![](./images/redécoupe.jpg)

![](./images/redécoupe-juste.jpg)

Après avoir redécoupé correctement la bache, il a fallut faire de même pour la doublure, pas pour le molletonage car il n'y en avait pas sur cette couche.

Etape 4:

On attache les deux premières couches ensemble.
on attache ensuite, seulement par le bas, la couche 3 au deux autres.

![](./images/attache-couches.jpg)
![](./images/attache-couches2.jpg)

Etape 5:

Confection des ceintures;
Pour la ceinture de gauche, on coud à l'envers les couches suivantes; bache, molletonage, bache. Ensuite on la retourne et on vient y mettre la sangle.

![](./images/ceinture-droite.jpg)

Pour la ceinture de droite, il faut prévoir une ouverture car celle-ci possedera une poche. On fait donc une incision de 20cm sur la bache, on y coud la tirette du côté intérieur.
Pour cette ceinture il faut donc mettre du doublage.

![](./images/ceinture-gauche.jpg)


Etape 6:

On coud la bande avec les tirettes sur l'envers du sac. on ne va pas jusqu'au bout car on doit y attacher la ceinture.  

![](./images/bande-attaché.jpg)

Entre ces deux coutures je viens disposer des sangles afin d'y mettre des attaches. elles serviront a fermer le sac.

Etape 7:

confection du voile pour recouvrir le sac et la canne. Pour cela j'avais découpé 2 rectangles égaux sur les bache. Celui du dessus sera orner de mon logo, a savoir un flotteur. il aura été découpé au lazer. Avant d'assembles les deu partie du voile je me charge du logo que j'aimerais mettre en couleur. Pour cela, je viens glisser entre les deux couches des morceaux de baches de couleur.  

![](./images/patch-logo.jpg)  ![](./images/logo.jpg)

Ensuite, sur la même bache je fais deux ouverture pour y passer des sangles.

Sur la bache inférieure, je vais coudre l'un des filet qui servira de poche supplémentaire. Entre les deux je met une couche de doublage, pour de raison esthétiques.

J'attache ensuite les deux bache ensemble.

![](./images/Voile.jpg)



Etape 8:

confection du fourreau pour la canne.
pour cela, je découpe un bande de 1m30 sur 12cm dans de la bache. Je fais de même pour la doublure et pour le molletonage, il est important que la canne soit bien protégée.
On attache le tout ensemble, cette fois dans le bon sens. je voudrait laisser les epaisseurs visible que je viens consolider grâce à colle adéquate.

![](./images/foureau.jpg)

Je viens attacher les languettes prédécoupé sur le fourreau afin de pouvoir refermer celui-ci.
je viens ensuite l'attacher au voile.



Etape 9:

On attache la ceinture de gauche et le voile, on coud toujours à l'envers du sac, on laisse cependant un espace au bout pour pouvoir retourer le sac. Ce trous sera comblé un fois le sac retourné avec une languette découpé dans les baches.


Etape 10:

on attache le fourreau au voile.

Etape 11:

Cette étape est l'étape de mise au propre, on nettoie les files qui dépassent, on ajoute quelques éléments de détails comme des sangles ou des élastiques pour le camel-back, on vient aussi faire deux trous dans le sac, un pour le camel-bac et l'autre pour la poignée du moulinet.


Voici quelques photos du sac finis:


![](./images/shootingok1.jpg)
![](./images/shootingok2.jpg)
![](./images/shootingok3.jpg)
![](./images/shootingok4.jpg)
